-Install latest node.js into local pc.

-Open command line and choose your file path,where you want create a project file.

-After choose the file path, in the command line write down (npm install -g @vue/cli)

-After done the installation of vue cli,then to create a project folder write down (vue create [demo])

-After that it will require to Pick a preset and choose "Manually select features"

-Then under of the "Check the featurs needed for your project" please choose by pressing <Space-Bar> 'Router', 'Vuex', 'Linter/Formatter'.

-After that "User history mode for router?" and press y to yes

-Once it done, then "Pick a linter/formatter config:" and choose ESLint with error prevention only.

-Next, From "Pick additonal lint features:" choose Lint on save.

-From "Where do you prefer placing config for Bebel, PostCSS, ESLint, etc.?" go and choose "in dedicated config files".

-If you want/do not to save your present for future press "y/n" from "Save this as a preset for future project".

-After done the all installation, if you are using Visual Studio, then open the folder in it and also open the "Terminal" of visual studio.

-In terminal, write (npm i -s vuedraggable) to install.

-After installation done, inside the terminal write "npm run serve"

-After take some installation, it will show localhost port number [e.g local 8080]. Use this link and paste it n browser url bar and see the output. 